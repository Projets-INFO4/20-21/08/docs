CIRSTEA Paul  
DE MULATIER Jean-Théophane  
SOULARD Alexandre  
INFO4  

# **Follow up sheet** #

# Project INFO4, Group 8, Godot Game Engine et tables tactiles

# 25/01/2021 :
## The whole group
- Start of the grip of Godot Engine.

# 1/02/2021 :  
## The whole group
- Study of the touchscreen table.  
- Set up of Gitlab.  
- Use of Godot Engine and his programmation language.  
- Thinking about the idea of the game.  

# 08/02/2021 :  
## The whole group  
- Decision of the game.  
- Starting of coding the board of the game.  

# 22/02/2021 :  
## The whole group  
- Coding of a board of a game (not our game but it was to train ourselves).  

# 23/02/2021 :  
## The whole group  
- Making interactions between the player and the board.  
- Starting of coding the script for the player and the different cases.  

# 01/03/2021 :  
## The whole group  
- Creation of a new Class : Game which control all the game.  
- Adding a new type of case.  
- Starting of coding the final board of the game.  
- Creation of two players and now we can press "espace" to make them move from their case.  

# 02/03/2021 :  
## The whole group  
- Progress on the class diagram and we uploaded it.  
- Finish the rules of the game and we uploaded it.  
- Refactoring of the different class names.  

# 08/03/2021 :  
## The whole group  
- Improvement of the game.  
- Beginning of setting up of the network.  

# 09/03/2021 :  
## The whole group  
- Tests on the touchscreen tablets.  
- We looked for other problems on the touchscreen tablets.    

# 15/03/2021 :  
## Jean Théophane  
- Improvement of the code of the game.      
## Paul, Alexandre
- Coding a simpler game : Tic Tac Toe.      
- Trying to put the network in place for the game Tic Tac Toe.       

# 16/03/2021 :  
## Jean Théophane  
- Drawing the background of the board of the game.      
## Paul, Alexandre
- Continue trying to put the network in place for the game Tic Tac Toe.     
- Analysis examples of Godot Engine's games which are multiplayers games.       

# 22/03/2021 :  
## The whole group      
- Making tests on tables.       
## Jean Théophane  
- Finished the background of the board of the game.      
- Place cells on the background.        
- Change the cells' system.     
## Paul, Alexandre
- Put in place the synchronisation between the server and the client for the game Tic Tac Toe.     

# 23/03/2021 :       
## Jean Théophane  
- Implements the dice shaker.        
## Paul, Alexandre
- Making possible the connection between the client and the server with an IP address for Tic Tac Toe.    
- Tests between two computers.    
- Starting to make possible to play another game.        

# 29/03/2021 :       
## Jean Théophane  
- Fixed bug.               
- Creation of images for bonus and penalties.       
## Paul, Alexandre
- Finalisation of the network on the game Tic Tac Toe.    
- Setting up the network on our game.    
