CIRSTEA Paul  
DE MULATIER Jean-Théophane  
SOULARD Alexandre  
INFO4 

# Project INFO4, Group 8, Godot Game Engine et tables tactiles

Follow up sheet : https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/08/docs/-/blob/master/Godot_Game_Engine_et_tables_tactiles_info4_2020_2021.md

# How can we start the game ?

## Linux
In order to start the game you have to install Godot Game Engine.       
After that you need to be placed on the directory where the file : project.godot, is.       
In this directory you have to do this following line :      
`~/<path_to_your_Godot_version>/<version_of_Godot>` 	    
For example :       
`~/code$ ~/Téléchargements/Godot_v3.2.3-stable_x11.64` If the file project.godot is in the directory : code.
